package twitter;

import java.util.Date;

import twitter4j.*;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		final Twitter twitter = new TwitterFactory().getInstance();
		/*
		Date now = new Date();
		String latestStatus = "Hey @fib_was, we've just completed task #4 [timestamp: "+now+"]";
		Status status = twitter.updateStatus(latestStatus);
		System.out.println("Successfully updated the status to: " + status.getText());       
		*/
		/*
		User usr = twitter.showUser("fib_was");
		System.out.print(usr.getStatus().getText());
		Status status = twitter.retweetStatus(usr.getStatus().getId());
		*/
		
		/*
		 //Primer intent 
		 while (true) {
		    Query query = new Query("#barcelona");
		    QueryResult result = twitter.search(query);
		    for (Status status : result.getTweets()) {
		        System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
		    }
		    Thread.sleep(5000);
		}
		*/
		
		StatusListener myListener = new StatusListener() {

			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatus(Status status) {
		        System.out.println(status.getUser().getName() + " (@" + status.getUser().getScreenName()+"): " + status.getText());
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}
			
		};
		TwitterStreamFactory sTw = new TwitterStreamFactory(); 
		TwitterStream stream = sTw.getInstance();
		FilterQuery fQuery = new FilterQuery();
		String vec[] = {"#barcelona"};
		fQuery.track(vec);
		stream.addListener(myListener);
		stream.filter(fQuery);
		
	}
}
