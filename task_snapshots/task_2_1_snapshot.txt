[
    {
        "id": 1181211139000213510,
        "id_str": "1181211139000213510",
        "name": "Xarxa en Enginyeria del Software de Catalunya",
        "screen_name": "XarxEs_CAT",
        "location": "",
        "description": "Coming soon ...",
        "url": "https://t.co/85TZnvlu9e",
        "entities": {
            "url": {
                "urls": [
                    {
                        "url": "https://t.co/85TZnvlu9e",
                        "expanded_url": "https://xarxescat.gessi.upc.edu",
                        "display_url": "xarxescat.gessi.upc.edu",
                        "indices": [
                            0,
                            23
                        ]
                    }
                ]
            },
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 4,
        "friends_count": 9,
        "listed_count": 0,
        "created_at": "Mon Oct 07 14:14:55 +0000 2019",
        "favourites_count": 0,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 0,
        "lang": null,
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/1182557686044471296/OHE4txiT_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/1182557686044471296/OHE4txiT_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/1181211139000213510/1570778100",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    },
    {
        "id": 452276847,
        "id_str": "452276847",
        "name": "BigData BARCELONA",
        "screen_name": "BigDataBCN",
        "location": "Barcelona",
        "description": "Interested in #BigData #FogComputing #IoT & #AI",
        "url": null,
        "entities": {
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 140,
        "friends_count": 505,
        "listed_count": 4,
        "created_at": "Sun Jan 01 17:02:03 +0000 2012",
        "favourites_count": 130,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 142,
        "lang": null,
        "status": {
            "created_at": "Sun Oct 13 08:45:25 +0000 2019",
            "id": 1183302711552421888,
            "id_str": "1183302711552421888",
            "text": "RT @JordiTorresAI: Brillante artículo de @nurianavarroe en LaContra  @elperiodico con reflexiones en #InteligenciaArtificial de @xamat  htt…",
            "truncated": false,
            "entities": {
                "hashtags": [
                    {
                        "text": "InteligenciaArtificial",
                        "indices": [
                            101,
                            124
                        ]
                    }
                ],
                "symbols": [],
                "user_mentions": [
                    {
                        "screen_name": "JordiTorresAI",
                        "name": "Jordi TORRES.AI",
                        "id": 113907485,
                        "id_str": "113907485",
                        "indices": [
                            3,
                            17
                        ]
                    },
                    {
                        "screen_name": "nurianavarroe",
                        "name": "Núria Navarro",
                        "id": 406605950,
                        "id_str": "406605950",
                        "indices": [
                            41,
                            55
                        ]
                    },
                    {
                        "screen_name": "elperiodico",
                        "name": "El Periódico",
                        "id": 198829810,
                        "id_str": "198829810",
                        "indices": [
                            69,
                            81
                        ]
                    },
                    {
                        "screen_name": "xamat",
                        "name": "Xavier Amatriain",
                        "id": 9316452,
                        "id_str": "9316452",
                        "indices": [
                            128,
                            134
                        ]
                    }
                ],
                "urls": []
            },
            "source": "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
            "in_reply_to_status_id": null,
            "in_reply_to_status_id_str": null,
            "in_reply_to_user_id": null,
            "in_reply_to_user_id_str": null,
            "in_reply_to_screen_name": null,
            "geo": null,
            "coordinates": null,
            "place": null,
            "contributors": null,
            "retweeted_status": {
                "created_at": "Sun Oct 13 08:19:31 +0000 2019",
                "id": 1183296191091675136,
                "id_str": "1183296191091675136",
                "text": "Brillante artículo de @nurianavarroe en LaContra  @elperiodico con reflexiones en #InteligenciaArtificial de @xamat  https://t.co/mI5tM4T1T7",
                "truncated": false,
                "entities": {
                    "hashtags": [
                        {
                            "text": "InteligenciaArtificial",
                            "indices": [
                                82,
                                105
                            ]
                        }
                    ],
                    "symbols": [],
                    "user_mentions": [
                        {
                            "screen_name": "nurianavarroe",
                            "name": "Núria Navarro",
                            "id": 406605950,
                            "id_str": "406605950",
                            "indices": [
                                22,
                                36
                            ]
                        },
                        {
                            "screen_name": "elperiodico",
                            "name": "El Periódico",
                            "id": 198829810,
                            "id_str": "198829810",
                            "indices": [
                                50,
                                62
                            ]
                        },
                        {
                            "screen_name": "xamat",
                            "name": "Xavier Amatriain",
                            "id": 9316452,
                            "id_str": "9316452",
                            "indices": [
                                109,
                                115
                            ]
                        }
                    ],
                    "urls": [
                        {
                            "url": "https://t.co/mI5tM4T1T7",
                            "expanded_url": "https://www.elperiodico.com/es/la-contra/20191013/la-contra-nuria-navarro-entrevista-xavier-amatriain-algotimo-netflix-7674515",
                            "display_url": "elperiodico.com/es/la-contra/2…",
                            "indices": [
                                117,
                                140
                            ]
                        }
                    ]
                },
                "source": "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
                "in_reply_to_status_id": null,
                "in_reply_to_status_id_str": null,
                "in_reply_to_user_id": null,
                "in_reply_to_user_id_str": null,
                "in_reply_to_screen_name": null,
                "geo": null,
                "coordinates": null,
                "place": {
                    "id": "03bcfc28ab5492ce",
                    "url": "https://api.twitter.com/1.1/geo/id/03bcfc28ab5492ce.json",
                    "place_type": "city",
                    "name": "Argentona",
                    "full_name": "Argentona, España",
                    "country_code": "ES",
                    "country": "Spain",
                    "contained_within": [],
                    "bounding_box": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [
                                    2.3562837,
                                    41.5304198
                                ],
                                [
                                    2.4268281,
                                    41.5304198
                                ],
                                [
                                    2.4268281,
                                    41.5955195
                                ],
                                [
                                    2.3562837,
                                    41.5955195
                                ]
                            ]
                        ]
                    },
                    "attributes": {}
                },
                "contributors": null,
                "is_quote_status": false,
                "retweet_count": 3,
                "favorite_count": 5,
                "favorited": false,
                "retweeted": false,
                "possibly_sensitive": false,
                "lang": "es"
            },
            "is_quote_status": false,
            "retweet_count": 3,
            "favorite_count": 0,
            "favorited": false,
            "retweeted": false,
            "lang": "es"
        },
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/877862589899960321/20oqpnJp_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/877862589899960321/20oqpnJp_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/452276847/1567931912",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    },
    {
        "id": 279544013,
        "id_str": "279544013",
        "name": "𝗝𝗼𝘀𝗲 𝗠𝗮𝗿𝗶𝗮 𝗣𝘂𝗲𝗿𝘁𝗮 🇪🇺",
        "screen_name": "josemariapuerta",
        "location": "Insula Maior, Mare Nostrum",
        "description": "𝗚𝗲𝗻𝘁𝗹𝗲 𝗖𝘆𝗻𝗶𝗰. ​Innovating is more than an attitude. Let's talk! Innovar es más que una actitud. ¡Hablemos! #heforshe #travel #innovation",
        "url": "https://t.co/GySeo1D7fD",
        "entities": {
            "url": {
                "urls": [
                    {
                        "url": "https://t.co/GySeo1D7fD",
                        "expanded_url": "http://josemariapuerta.xyz",
                        "display_url": "josemariapuerta.xyz",
                        "indices": [
                            0,
                            23
                        ]
                    }
                ]
            },
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 3905,
        "friends_count": 2846,
        "listed_count": 228,
        "created_at": "Sat Apr 09 13:48:49 +0000 2011",
        "favourites_count": 22023,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 30765,
        "lang": null,
        "status": {
            "created_at": "Tue Oct 22 10:27:02 +0000 2019",
            "id": 1186589773760290817,
            "id_str": "1186589773760290817",
            "text": "RT @theworldindex: Bloomberg 2019 Healthiest Country Index:\n\n1.🇪🇸Spain\n2.🇮🇹Italy\n3.🇮🇸Iceland\n4.🇯🇵Japan\n5.🇨🇭Switzerland\n6.🇸🇪Sweden\n7.🇦🇺Austr…",
            "truncated": false,
            "entities": {
                "hashtags": [],
                "symbols": [],
                "user_mentions": [
                    {
                        "screen_name": "theworldindex",
                        "name": "World Index",
                        "id": 938419883745869824,
                        "id_str": "938419883745869824",
                        "indices": [
                            3,
                            17
                        ]
                    }
                ],
                "urls": []
            },
            "source": "<a href=\"https://buffer.com\" rel=\"nofollow\">Buffer</a>",
            "in_reply_to_status_id": null,
            "in_reply_to_status_id_str": null,
            "in_reply_to_user_id": null,
            "in_reply_to_user_id_str": null,
            "in_reply_to_screen_name": null,
            "geo": null,
            "coordinates": null,
            "place": null,
            "contributors": null,
            "retweeted_status": {
                "created_at": "Sat Oct 19 14:51:00 +0000 2019",
                "id": 1185569037587243010,
                "id_str": "1185569037587243010",
                "text": "Bloomberg 2019 Healthiest Country Index:\n\n1.🇪🇸Spain\n2.🇮🇹Italy\n3.🇮🇸Iceland\n4.🇯🇵Japan\n5.🇨🇭Switzerland\n6.🇸🇪Sweden\n7.🇦🇺… https://t.co/oK0TshC1IW",
                "truncated": true,
                "entities": {
                    "hashtags": [],
                    "symbols": [],
                    "user_mentions": [],
                    "urls": [
                        {
                            "url": "https://t.co/oK0TshC1IW",
                            "expanded_url": "https://twitter.com/i/web/status/1185569037587243010",
                            "display_url": "twitter.com/i/web/status/1…",
                            "indices": [
                                117,
                                140
                            ]
                        }
                    ]
                },
                "source": "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
                "in_reply_to_status_id": null,
                "in_reply_to_status_id_str": null,
                "in_reply_to_user_id": null,
                "in_reply_to_user_id_str": null,
                "in_reply_to_screen_name": null,
                "geo": null,
                "coordinates": null,
                "place": null,
                "contributors": null,
                "is_quote_status": false,
                "retweet_count": 1497,
                "favorite_count": 3484,
                "favorited": false,
                "retweeted": false,
                "lang": "en"
            },
            "is_quote_status": false,
            "retweet_count": 1497,
            "favorite_count": 0,
            "favorited": false,
            "retweeted": false,
            "lang": "en"
        },
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "000000",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/1150656113571971074/ByYAUtLv_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/1150656113571971074/ByYAUtLv_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/279544013/1528370802",
        "profile_link_color": "0073FF",
        "profile_sidebar_border_color": "000000",
        "profile_sidebar_fill_color": "1A3F57",
        "profile_text_color": "72B9BF",
        "profile_use_background_image": true,
        "has_extended_profile": true,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    },
    {
        "id": 600045110,
        "id_str": "600045110",
        "name": "Mikl14",
        "screen_name": "Miklx14",
        "location": "",
        "description": "Usuari habitual de Rodalies, per desgràcia 😖",
        "url": null,
        "entities": {
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 16,
        "friends_count": 85,
        "listed_count": 0,
        "created_at": "Tue Jun 05 11:47:23 +0000 2012",
        "favourites_count": 316,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 492,
        "lang": null,
        "status": {
            "created_at": "Thu Oct 03 19:26:12 +0000 2019",
            "id": 1179840091042275330,
            "id_str": "1179840091042275330",
            "text": "RT @NVIDIAGeForceES: Para celebrar el lanzamiento de Destiny 2 Shadowkeep en Steam, tenemos esta Edición Coleccionista para PC y... ¡la est…",
            "truncated": false,
            "entities": {
                "hashtags": [],
                "symbols": [],
                "user_mentions": [
                    {
                        "screen_name": "NVIDIAGeForceES",
                        "name": "NVIDIA GeForce ES",
                        "id": 93843845,
                        "id_str": "93843845",
                        "indices": [
                            3,
                            19
                        ]
                    }
                ],
                "urls": []
            },
            "source": "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
            "in_reply_to_status_id": null,
            "in_reply_to_status_id_str": null,
            "in_reply_to_user_id": null,
            "in_reply_to_user_id_str": null,
            "in_reply_to_screen_name": null,
            "geo": null,
            "coordinates": null,
            "place": null,
            "contributors": null,
            "retweeted_status": {
                "created_at": "Thu Oct 03 15:07:43 +0000 2019",
                "id": 1179775039866060800,
                "id_str": "1179775039866060800",
                "text": "Para celebrar el lanzamiento de Destiny 2 Shadowkeep en Steam, tenemos esta Edición Coleccionista para PC y... ¡la… https://t.co/BeOuvYMTV4",
                "truncated": true,
                "entities": {
                    "hashtags": [],
                    "symbols": [],
                    "user_mentions": [],
                    "urls": [
                        {
                            "url": "https://t.co/BeOuvYMTV4",
                            "expanded_url": "https://twitter.com/i/web/status/1179775039866060800",
                            "display_url": "twitter.com/i/web/status/1…",
                            "indices": [
                                116,
                                139
                            ]
                        }
                    ]
                },
                "source": "<a href=\"https://prod1.sprinklr.com\" rel=\"nofollow\">Sprinklr Publishing</a>",
                "in_reply_to_status_id": null,
                "in_reply_to_status_id_str": null,
                "in_reply_to_user_id": null,
                "in_reply_to_user_id_str": null,
                "in_reply_to_screen_name": null,
                "geo": null,
                "coordinates": null,
                "place": null,
                "contributors": null,
                "is_quote_status": false,
                "retweet_count": 2324,
                "favorite_count": 590,
                "favorited": false,
                "retweeted": false,
                "possibly_sensitive": false,
                "lang": "es"
            },
            "is_quote_status": false,
            "retweet_count": 2324,
            "favorite_count": 0,
            "favorited": false,
            "retweeted": false,
            "lang": "es"
        },
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/2504561140/94329_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/2504561140/94329_normal.jpg",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    },
    {
        "id": 41112209,
        "id_str": "41112209",
        "name": "VerbK",
        "screen_name": "vddr",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 89,
        "friends_count": 2730,
        "listed_count": 0,
        "created_at": "Tue May 19 13:13:53 +0000 2009",
        "favourites_count": 5258,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 308,
        "lang": null,
        "status": {
            "created_at": "Sun Oct 13 08:16:24 +0000 2019",
            "id": 1183295406400098306,
            "id_str": "1183295406400098306",
            "text": "@MBrundleF1 Please come back soon",
            "truncated": false,
            "entities": {
                "hashtags": [],
                "symbols": [],
                "user_mentions": [
                    {
                        "screen_name": "MBrundleF1",
                        "name": "Martin Brundle",
                        "id": 239172505,
                        "id_str": "239172505",
                        "indices": [
                            0,
                            11
                        ]
                    }
                ],
                "urls": []
            },
            "source": "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iΟS</a>",
            "in_reply_to_status_id": 1183293399878832130,
            "in_reply_to_status_id_str": "1183293399878832130",
            "in_reply_to_user_id": 239172505,
            "in_reply_to_user_id_str": "239172505",
            "in_reply_to_screen_name": "MBrundleF1",
            "geo": null,
            "coordinates": null,
            "place": null,
            "contributors": null,
            "is_quote_status": false,
            "retweet_count": 0,
            "favorite_count": 1,
            "favorited": false,
            "retweeted": false,
            "lang": "en"
        },
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/683125808903356416/UC6FcfUn_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/683125808903356416/UC6FcfUn_normal.png",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    }
]
